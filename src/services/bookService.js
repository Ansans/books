import bookDao from "../dao/bookDao.js";

const findAll = async () => {
  const books = await bookDao.findAll();
  if (books.rows !== 0) {
    return books.rows;
  } else {
    const err = new Error(`There are no books!`);
    err.status = "fail";
    err.statusCode = 404;
  }
};

const validateBook = (book) => {
  return book["name"] && book["author"];
};

const insertBook = async (book) => {
  console.log(book);
  if (!validateBook(book)) {
    const err = new Error("Give a name and an author.");
    err.name = "UserError";
    throw err;
  } else {
    return await bookDao.insertBook(book);
  }
};

const updateBook = async (book) => {
  if (book === undefined) {
    const err = new Error("No such book found.");
    err.name = "UserError";
    throw err;
  } else {
    return await bookDao.updateBook(book);
  }
};

const findOne = async (id) => {
  const book = await bookDao.findOne(id);
  if (book == undefined) {
    const err = new Error("No such book found.");
    err.name = "UserError";
    throw err;
  } else {
    return book;
  }
};

export default {
  findAll,
  findOne,
  insertBook,
  updateBook,
};
