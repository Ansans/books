// import { v4 as uuidv4 } from 'uuid'
import { executeQuery } from "../db/pg.js";
import bookService from "../services/bookService.js";

const findAll = async () => {
  console.log("Searching for books...");
  const result = await executeQuery('SELECT * FROM "books";');
  console.log(`Found ${result.rows.length} books.`);
  return result;
};

const insertBook = async (book) => {
  const params = [...Object.values(book)];
  console.log(`Inserting a new book ${params[0]}...`);
  const result = await executeQuery(
    'INSERT INTO "books" ("name","author","read") VALUES ($1, $2, $3);',
    params
  );
  console.log(`New book ${params[0]} inserted successfully.`);
  return result;
};

const updateBook = async (book) => {
  const params = [book.name, book.author, book.read];
  console.log(`Updating a book ${params[0]}...`);
  const result = await executeQuery(
    'UPDATE "books" SET name=$1, author=$2 WHERE read=$3;',
    params
  );
  console.log(`Product ${params[0]} updated successfully.`);
  return result;
};

const findOne = async (id) => {
  console.log(`Requesting a product with id: ${id}...`);
  const result = await executeQuery('SELECT * FROM "books" WHERE id = $1;', [
    id,
  ]);

  return result.rows[0];
};

export default {
  findAll,
  findOne,
  insertBook,
  updateBook,
};
