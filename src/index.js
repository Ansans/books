import express from "express";
import bookRouter from "./api/bookRouter.js";
import "express-async-errors";
import dotenv from "dotenv";
import { createTables } from "./db/pg.js";
import { unknownEndpoint, errorHandler } from "./middlewares.js";

process.env.NODE_ENV === "env" && dotenv.config();
const APP_PORT = process.env.APP_PORT || 8080;

const app = express();
app.use(express.json());
app.use("/books", bookRouter);
app.use(unknownEndpoint);
app.use(errorHandler);

process.env.NODE_ENV !== "test" &&
  createTables() &&
  app.listen(APP_PORT, () => {
    console.log(`Listening to ${APP_PORT}.`);
  });

export default app;
