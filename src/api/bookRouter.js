import express from "express";
import { Router } from "express";
import bookService from "../services/bookService.js";

const router = Router();

// Kaikki kirjat
router.get("/", async (req, res, next) => {
  try {
    const books = await bookService.findAll();
    res.status(200).json(books);
  } catch (error) {
    next(error);
  }
});

// Lisää kirja
router.post("/", async (req, res, next) => {
  try {
    const book = req.body;
    await bookService.insertBook(book);
    res.status(201).send("Created");
  } catch (error) {
    next(error);
  }
});

// Tietty kirja
router.get("/:id", async (req, res, next) => {
  try {
    const book = await bookService.findOne(req.params.id);
    res.status(200).json(book);
  } catch (error) {
    next(error);
  }
});

// Muokkaa kirjaa
router.put("/:id", async (req, res, next) => {
  try {
    const book = { ...req.body };
    const storedBook = await bookService.updateBook(book);
    res.status(200).send("Updated");
  } catch (error) {
    next(error);
  }
});

export default router;
