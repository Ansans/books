export const unknownEndpoint = (_req, res) => {
  res.status(404).send({ error: "No one here" });
};

export const errorHandler = (error, req, res, next) => {
  console.log(error);
  if (error.name === "UserError") {
    res.status(400).send({ error: error.message });
  } else if (error.name === "dbError") {
    res.status(500).send({ error: error.message });
  }
  next(error);
};
