const createBookTable = `
CREATE TABLE IF NOT EXISTS "books" (
    "id" SERIAL,
    "name" VARCHAR(100) NOT NULL,
    "author" VARCHAR(100) NOT NULL,
    "read" BOOLEAN NOT NULL,
    PRIMARY KEY ("id")

);`;

export default { createBookTable };
