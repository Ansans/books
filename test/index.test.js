import request from "supertest";
import { jest } from "@jest/globals";
import app from "../src/index.js";
import { pool } from "../src/db/pg.js";

const initializeDbMock = (expectedResponse) => {
  pool.connect = jest.fn(() => {
    return {
      query: () => expectedResponse,
      release: () => null,
    };
  });
};

describe("Testing GET", () => {
  const mockResponse = {
    rows: [
      { id: "1", name: "Kirja 1", author: "kirjailija", read: true },
      { id: "2", name: "Kirja 2", author: "kirjailija 2", read: false },
    ],
  };

  beforeAll(() => {
    initializeDbMock(mockResponse);
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it("Returns 200 when getting all the books", async () => {
    const response = await request(app)
      .get("/books")
      .set("Content-Type", "application/json");
    expect(response.status).toBe(200);
    expect(JSON.parse(response.text)).toStrictEqual(mockResponse.rows);
  });

  it("Returns 200 when finding a certain book by id", async () => {
    const mockResponse = {
      rowCount: 1,
      rows: [{ id: "1", name: "kirja", author: "joku" }],
    };
    initializeDbMock(mockResponse);
    const response = await request(app).get("/books/1");
    expect(response.status).toBe(200);
    expect(JSON.parse(response.text)).toStrictEqual(mockResponse.rows[0]);
  });

  // it('Returns 500 with faulty database behaviour', async () => {
  // const response = await request(app)
  // .get('/books/xyz')
  // .set('Content-Type', 'application/json')
  // expect(response.status).toBe(500)
  // })

  describe("Testing POST /products", () => {
    const mockResponse = {
      rowCount: 1,
    };

    beforeAll(() => {
      initializeDbMock(mockResponse);
    });

    afterAll(() => {
      jest.clearAllMocks();
    });

    it("Returns 201 with valid new product", async () => {
      const response = await request(app)
        .post("/books")
        .send({ name: "kirja", author: "kirjailija" })
        .set("Content-Type", "application/json");
      expect(response.status).toBe(201);
    });
  });

  // it('Returns 400 when a book is not found', async () => {
  //   const mockResponse = {
  //       rowCount: 1,
  //       rows: [
  //           { id: '1', name: "kirja", author: "joku" }
  //       ]
  //   }
  //   initializeDbMock(mockResponse)

  //     const response = await request(app)
  //     .get('/books/12345')
  //     expect(response.status).toBe(400)

  // })

  describe("Testing PUT /books", () => {
    const mockResponse = {
      rows: [
        { id: "1", name: "Kirja 1", author: "kirjailija", read: true },
        { id: "2", name: "Kirja 2", author: "kirjailija 2", read: false },
      ],
    };

    beforeAll(() => {
      initializeDbMock(mockResponse);
    });

    afterAll(() => {
      jest.clearAllMocks();
    });

    it("Returns 200 when a book is updated", async () => {
      const response = await request(app)
        .put("/books/1")
        .send({ name: "kirja", author: "kirjailija" })
        .set("Content-Type", "application/json");
      expect(response.status).toBe(200);
    });

    //   it('Returns 400 when a book is not updated', async () => {
    //     const response = await request(app)
    //     .put('/books/1')
    //     .send({})
    //     .set('Content-Type', 'application/json')
    //     expect(response.status).toBe(400)
    // })
  });
});
